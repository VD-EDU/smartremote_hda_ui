var provdata;
var provdataview;
$(function(){
$('.faceplateon1').click(function() {
$("#faceplate1").attr('src',"faceplateonm1.png");
return false;
});
});
$(function(){
$('.faceplateoff1').click(function(){
$("#faceplate1").attr('src',"faceplate111.png");
return false;
});
});
$(function(){
$('.faceplateon2').click(function(){
$("#faceplate1").attr('src',"faceplateonm2.png");
return false;
});
});
$(function(){
$('.faceplateoff2').click(function(){
$("#faceplate1").attr('src',"faceplate111.png");
return false;
});
});
attachProvBtnCB = function(provdata, what) {
    var label;
	if (what === "reset_to_prov") {
		label = "Reset WiFi Settings";
		$("#prov-btn").unbind();
		$("#prov-btn").bind('click', function() {
			provdataview = prov_show(provdata, "reset_to_prov");
		});
	} else if (what === "prov") {
		label = "WiFi Setup";
		$("#prov-btn").unbind();
		$("#prov-btn").bind('click', function() {
            provdataview = prov_show(provdata, "prov");
		});
	} else {
		return;
	}
	$("#div-prov-btn ul li a").html(label);
	$("#div-prov-btn").show();
}

var createHTMLPage = function() {
	$("#header #back").hide();
	$("#header #home").hide();
	$("#page_content").html("");
	$("#header #title").text("uControl Remote Device Settings");
        $("#page_content").append($("<ul/>", {"data-role":"listview","data-inset":true, "data-divider-theme":"d", "id":"ps-control"})
						.append($("<li/>", {"data-role":"list-divider"})
                                                .append($("<div/>",{"id":"name","align":"center"})
						.append("")))
						.append($("<li/>", {"data-role":"fieldcontain", "data-theme":"c"})
						.append($("<label/>", {"for":"power-switch-flip"})
                                                //.append("Switch State"))
                                                .append($("<div/>",{"id":"img","align":"center"})
                                                .append($("<img/>",{"src":"faceplate111.png","width":"100px","height":"98px","usemap":"faceplate","id":"faceplate1"})
                                                .append($("<map/>",{"id":"faceplate1","name":"faceplate"})
                                                .append($("<area/>",{"shape":"rect","coords":"56,80,106,130","href":"","alt":"On","title":"On", "class":"faceplateon1"}))
                                                .append($("<area/>",{"shape":"rect","coords":"57,187,107,237","href":"","alt":"On","title":"Off", "class":"faceplateoff1"}))
                                                .append($("<area/>",{"shape":"rect","coords":"195,78,245,128","href":"","alt":"On","title":"On", "class":"faceplateon2"}))
                                                .append($("<area/>",{"shape":"rect","coords":"193,192,243,242","href":"","alt":"On","title":"Off", "class":"faceplateoff2"})))))
                                                )));
	$("#page_content").append($("<div/>", {"id":"div-prov-btn"})
			.append($("<ul/>", {"data-role":"listview","data-inset":true, "data-divider-theme":"d", id: "ul-prov-btn"})
			.append($("<li/>", {"data-role":"fieldcontain", "data-theme":"c"})
			.append($("<a/>", {"href":"#", "id":"prov-btn", "data-theme":"c", "data-shadow":"false"})
			.append()))));    
	$("#div-prov-btn").hide();
//Added Footer
	$("#footer #first").hide();
	$("#footer #second").hide();
	$("#footer").append($("<div/>",{"id":"href","align":"right"})
		.append($("<a/>",{"href":"http://www.vadactro.org.in"})
		.append("Powered By")
		.append($("<img/>",{"src":"logo11.png","width":"100px","heigth":"60px"}))
	))
};

var show = function() {
	createHTMLPage(); 
	
	provdata = prov_init();
	provdata.changedState.attach(function(obj, state) {
		if (obj.state === "unconfigured") {
			attachProvBtnCB(provdata, "prov");
		}
		else if (obj.state === "configured") {
			attachProvBtnCB(provdata, "reset_to_prov");
		}
	});
}

var pageLoad = function() {

        commonInit();
	$("#home").bind("click", function() {
		provdataview.hide();
		provdata.destroy();
		show();
	});

	show();
}
